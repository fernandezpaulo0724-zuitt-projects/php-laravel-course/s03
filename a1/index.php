<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activity 03</title>
</head>
<body>

    <h1>Person</h1>
    <p><?php echo $personOne->printName();?></p>

    <h1>Developer</h1>
    <p></p>
    <p><?php echo $personTwo->printName();?></p>
    <h1>Engineer</h1>
    <p><?php echo $personThree->printName();?></p></p>
</body>
</html>